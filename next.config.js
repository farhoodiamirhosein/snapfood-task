/** @type {import('next').NextConfig} */

const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "cdn.snappfood.ir",
        port: "",
      },
      {
        protocol: "https",
        hostname: "www.zoodfood.com",
        port: "",
      },
    ],
  },
};

module.exports = nextConfig;
