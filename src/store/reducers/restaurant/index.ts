import { RestaurantItemType } from "@/apis/restaurants";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface RestaurantState {
  restaurants: RestaurantItemType[];
  page: number;
  loading: boolean;
}

const initialState: RestaurantState = {
  restaurants: [],
  page: 0,
  loading: false,
};

const restaurantSlice = createSlice({
  name: "restaurant",
  initialState,
  reducers: {
    updateRestaurants: (
      state,
      action: PayloadAction<{ restaurants: RestaurantItemType[]; page: number }>
    ) => {
      state.restaurants = [...state.restaurants, ...action.payload.restaurants];
      state.page = action.payload.page;
      state.loading = false;
    },
    setRestaurantsLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
  },
});

export const { updateRestaurants, setRestaurantsLoading } =
  restaurantSlice.actions;
export default restaurantSlice.reducer;
