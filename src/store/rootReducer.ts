import { combineReducers } from "@reduxjs/toolkit";
import restaurantReducer from "./reducers/restaurant";

const rootReducer = combineReducers({
  restaurant: restaurantReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
