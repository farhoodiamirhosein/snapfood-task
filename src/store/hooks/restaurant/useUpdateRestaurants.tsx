/**
 * This custom hook fetches data from API and updates the redux state.
 */

import { getRestaurants } from "@/apis/restaurants";
import {
  updateRestaurants,
  setRestaurantsLoading,
} from "@/store/reducers/restaurant";
import { useCallback } from "react";
import { useDispatch } from "react-redux";

const UseUpdateRestaurants = () => {
  const dispatch = useDispatch();

  const fetchAndUpdateData = useCallback(
    async (page: number) => {
      dispatch(setRestaurantsLoading(true));
      let response = await getRestaurants({ page });
      dispatch(
        updateRestaurants({ restaurants: response.data.finalResult, page })
      );
    },
    [dispatch]
  );

  return fetchAndUpdateData;
};

export default UseUpdateRestaurants;
