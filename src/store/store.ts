import { configureStore } from "@reduxjs/toolkit";
import restaurantReducer from "./reducers/restaurant";

const store = configureStore({
  reducer: {
    restaurant: restaurantReducer,
  },
  devTools: process.env.NODE_ENV !== "production",
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
