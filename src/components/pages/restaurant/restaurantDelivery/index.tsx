import Style from "./index.style";
import numberSeparator from "@/utils/numberSeparator";
const RestaurantDelivery = ({
  deliveryFee,
  isExpress,
}: {
  deliveryFee: number;
  isExpress: boolean;
}) => {
  return (
    <Style>
      <div className="delivery_type">
        <div className="delivery_type_value">
          {isExpress ? "ارسال اکسپرس" : "پیک فروشنده"}
        </div>
        <div className="delivery_fee">
          {deliveryFee && deliveryFee > 0
            ? `${numberSeparator(deliveryFee, 3, ",")} تومان`
            : "رایگان"}
        </div>
      </div>
    </Style>
  );
};

export default RestaurantDelivery;
