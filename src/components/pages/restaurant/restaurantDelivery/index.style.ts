import { styled } from "styled-components";

const Style = styled.div`
  .delivery_type {
    display: flex;
    align-items: center;
    font-size: var(--text-small);
    font-weight: var(--font-regular);
    color: var(--text-color);

    .delivery_type_value {
      margin-left: 4px;
      font-size: var(--text-small);
      font-weight: var(--font-regular);
      color: var(--text-color-light);
    }
  }
`;

export default Style;
