import { styled } from "styled-components";

const Style = styled.div<{ $ispro: boolean }>`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  font-size: var(--text-small);
  font-weight: var(--font-medium);
  color: ${(props) =>
    !props.$ispro ? "var(--primary-deep-green)" : "var(--primary-color)"};
  border-radius: 16px 0 0 16px;
  padding: 4px 8px;
  background-color: #fff;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  gap: 4px;
  .ispro_icon_container {
    display: flex;
  }
`;

export default Style;
