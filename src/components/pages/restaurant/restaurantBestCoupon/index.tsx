import Style from "./index.style";
import Image from "next/image";
const RestaurantBestCoupon = ({
  title,
  isPro,
}: {
  title: string;
  isPro: boolean;
}) => {
  return (
    <Style $ispro={isPro}>
      {isPro && (
        <div className="ispro_icon_container">
          <Image
            priority
            alt={"pro icon"}
            src={"/images/icons/pro.svg"}
            width={20}
            height={11}
          />
        </div>
      )}
      <span>{title}</span>
    </Style>
  );
};

export default RestaurantBestCoupon;
