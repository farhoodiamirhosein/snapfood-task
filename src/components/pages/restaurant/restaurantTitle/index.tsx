import PercentageBadge from "@/components/uielements/discountBadge";
import Style from "./index.style";
import Image from "next/image";
import RateBadge from "@/components/uielements/rateBadge";
import TextBadge from "@/components/uielements/textBadge";
import numberSeparator from "@/utils/numberSeparator";

const restaurantTitle = ({
  title,
  discount,
  rate,
  voteCount,
  foodParty,
}: {
  title: string;
  discount: number;
  voteCount: number;
  rate: number;
  foodParty: boolean;
}) => {
  return (
    <Style>
      <div className="title_info">
        <span className="title_info_text">{title}</span>
        {discount && discount > 0 ? <PercentageBadge value={discount} /> : ""}
        {foodParty && (
          <Image
            alt="foodparty"
            src="/images/icons/foodparty.svg"
            width={14}
            height={14}
          />
        )}
      </div>
      {voteCount >= 20 ? (
        <div className="title_rate">
          <div className="title_rate_votecount">
            {`(${numberSeparator(voteCount, 3, ",")})`}
          </div>
          <RateBadge value={rate} />
        </div>
      ) : (
        <TextBadge
          text="جدید"
          bgColor="rgba(34,169,88,0.06)"
          color="var(--text-green)"
        />
      )}
    </Style>
  );
};

export default restaurantTitle;
