import { styled } from "styled-components";

const Style = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .title_info {
    display: flex;
    align-items: center;
    gap: 4px;
    span {
      font-size: var(--text-medium);
      font-weight: var(--font-medium);
      color: var(--text-color);
    }
  }
  .title_info_text {
    margin-left: 4px;
  }
  .title_rate {
    display: flex;
    align-items: center;
  }
  .title_rate_votecount {
    font-size: var(--text-small);
    font-weight: var(--font-regular);
    color: var(--text-color-light);
  }
`;

export default Style;
