import { styled } from "styled-components";

const Style = styled.div`
  width: 100%;

  .infinite_scroll_container {
    max-width: 100%;
    display: flex;
    flex-direction: column;
    gap: 16px;
    padding: 8px;
    align-items: center;
  }
`;

export default Style;
