"use client";

import { RestaurantItemType } from "@/apis/restaurants";
import { updateRestaurants } from "@/store/reducers/restaurant";
import { RootState } from "@/store/rootReducer";
import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import RestaurantCard from "../restaurantCard";
import Style from "./index.style";
import InfiniteScroll from "react-infinite-scroll-component";
import UseUpdateRestaurants from "@/store/hooks/restaurant/useUpdateRestaurants";

const RestaurantsList = ({
  data,
}: {
  data: { data: { finalResult: RestaurantItemType[] } };
}) => {
  const dispatch = useDispatch();
  const { restaurants, page } = useSelector(
    (state: RootState) => state.restaurant
  );

  useEffect(() => {
    dispatch(
      updateRestaurants({ restaurants: data.data.finalResult, page: 0 })
    );
  }, [data, dispatch]);

  const fetchAndUpdateRestaurants = UseUpdateRestaurants();

  const handleLoadMore = useCallback(() => {
    fetchAndUpdateRestaurants(page + 1);
  }, [fetchAndUpdateRestaurants, page]);

  return restaurants ? (
    <Style>
      <InfiniteScroll
        dataLength={restaurants.length}
        next={handleLoadMore}
        hasMore={true}
        loader={""}
        endMessage={
          <p style={{ textAlign: "center" }}>
            <b>به انتها رسیدید !!</b>
          </p>
        }
        className="infinite_scroll_container"
      >
        {restaurants.slice(1, restaurants.length).map((restaurant) => {
          return (
            <RestaurantCard
              data={restaurant.data}
              key={restaurant.data.id + ""}
            />
          );
        })}
      </InfiniteScroll>
    </Style>
  ) : (
    ""
  );
};

export default RestaurantsList;
