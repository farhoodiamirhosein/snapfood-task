import Image from "next/image";

const RestaurantCover = ({ backgroundImage }: { backgroundImage: string }) => {
  return (
    <Image
      src={backgroundImage}
      alt="section cover"
      sizes="100vw"
      quality={100}
      width={600}
      height={200}
      style={{
        width: "100%",
        height: "auto",
      }}
    />
  );
};

export default RestaurantCover;
