import Image from "next/image";
import Style from "./index.style";

const RestaurantProfileCard = ({
  src,
  alt,
  size = 56,
}: {
  src: string;
  alt: string;
  size: number;
}) => {
  return (
    <Style>
      <div className="card_logo">
        <Image src={src} alt={alt} width={size} height={size} />
      </div>
    </Style>
  );
};

export default RestaurantProfileCard;
