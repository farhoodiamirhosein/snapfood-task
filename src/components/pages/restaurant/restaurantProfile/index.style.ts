import { styled } from "styled-components";

const Style = styled.div`
  padding: 4px;
  border-radius: 8px;
  overflow: hidden;
  background: var(--white-color);
  z-index: 4;
  box-shadow: var(--card-shadow-3);
  .card_logo {
    border-radius: 4px;
    overflow: hidden;
    border: 1px solid var(--alpha-carbon-grey);
    display: flex;
  }
`;

export default Style;
