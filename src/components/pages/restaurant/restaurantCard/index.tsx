import { RestaurantType } from "@/apis/restaurants";
import Style from "./index.style";
import RestaurantProfile from "../restaurantProfile";
import Link from "next/link";
import RestaurantBestCoupon from "../restaurantBestCoupon";
import RestaurantCover from "../restaurantCover";
import RestaurantTitle from "../restaurantTitle";
import RestaurantDelivery from "../restaurantDelivery";

const RestaurantCard = ({ data }: { data: RestaurantType }) => {
  const {
    rate,
    voteCount,
    logo,
    menuUrl,
    backgroundImage,
    best_coupon,
    is_pro,
    discountValueForView,
    title,
    is_food_party,
    description,
    deliveryFee,
    isZFExpress,
  } = data;

  return (
    <Style>
      <Link href={menuUrl}>
        <header className="section_cover">
          <div className="section_cover_image">
            <RestaurantCover backgroundImage={backgroundImage} />
          </div>
          {best_coupon && (
            <div className="section_bestcoupon">
              <RestaurantBestCoupon title={best_coupon} isPro={is_pro} />
            </div>
          )}
          <div className="section_logo">
            <RestaurantProfile
              src={logo}
              size={56}
              alt={"restaurant profile"}
            />
          </div>
        </header>

        <div className="section_details">
          <div className="section_details_title">
            <RestaurantTitle
              title={title}
              discount={discountValueForView}
              voteCount={voteCount}
              rate={rate}
              foodParty={is_food_party}
            />
          </div>
          <div className="section_details_description">{description}</div>
          <div className="section_details_delivery">
            <RestaurantDelivery
              isExpress={isZFExpress}
              deliveryFee={deliveryFee}
            />
          </div>
        </div>
      </Link>
    </Style>
  );
};
export default RestaurantCard;
