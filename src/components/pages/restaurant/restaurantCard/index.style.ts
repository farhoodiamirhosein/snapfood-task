import { styled } from "styled-components";

const Style = styled.section`
  height: 248px;
  width: 565px;
  min-width: min(calc(100% - 4px), 260px);
  max-width: 100%;
  position: relative;
  border-radius: 6px;
  overflow: hidden;
  box-shadow: var(--card-shadow-3);

  a {
    text-decoration: unset;
  }

  .section_cover {
    height: 134px;
    width: 100%;
    position: relative;
  }

  .section_cover_image {
    height: 100%;
    width: 100%;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .section_cover_placeholder {
    height: 134px;
    width: 100%;
    background: lightblue;
  }

  .section_logo {
    position: absolute;
    right: 8px;
    bottom: -16px;
  }

  .section_bestcoupon {
    position: absolute;
    top: 8px;
    right: 0px;
  }

  .section_details {
    padding: 20px 16px 8px 16px;
    height: 114px;
    box-sizing: border-box;
  }

  .section_details_title {
    margin-bottom: 10px;
  }

  .section_details_description {
    font-size: var(--text-small);
    font-weight: var(--font-regular);
    color: var(--text-color);
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    flex-shrink: 0;
  }

  .section_details_delivery {
    margin-top: 10px;
  }
`;

export default Style;
