/**
 * This component displays rating depends on rate color.
 * @param {number} value - Number of received rate.
 */

import StarIcon from "../icons/star";
import Style from "./index.style";
const RateBadge = ({ value }: { value: number }) => {
  let relatedColor = "default";
  let relatedBgColor = "light-limeade-color";

  if (value <= 4.5) {
    relatedColor = "45";
  }

  if (value <= 4) {
    relatedColor = "40";
    relatedBgColor = "light-yellow-green-color";
  }

  if (value <= 3.5) {
    relatedColor = "35";
  }

  if (value <= 3) {
    relatedColor = "25";
    relatedBgColor = "light-selective-yellow-color";
  }

  if (value <= 2.5) {
    relatedColor = "20";
  }

  if (value <= 2) {
    relatedColor = "15";
  }

  if (value <= 1.5) {
    relatedColor = "10";
  }

  if (value <= 1) {
    relatedColor = "10";
  }

  const rateColor = `var(--starcolor-${relatedColor})`;
  const rateBgColor = `var(--${relatedBgColor})`;

  return (
    <Style $rateColor={rateColor} $rateBgcolor={rateBgColor}>
      <div className="rate_value">{value}</div>
      <StarIcon fill={rateColor} />
    </Style>
  );
};

export default RateBadge;
