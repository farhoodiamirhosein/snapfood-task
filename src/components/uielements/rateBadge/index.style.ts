import { styled } from "styled-components";

const Style = styled.div<{ $rateColor: string; $rateBgcolor: string }>`
  background: ${(props) => props.$rateBgcolor};
  display: flex;
  flex-direction: row;
  padding: 0px 2px 0px 2px;
  border-radius: 4px;
  align-items: center;
  svg {
    padding-bottom: 1px;
  }
  .rate_value {
    font-size: var(--text-small);
    font-weight: var(--font-regular);
    color: ${(props) => props.$rateColor};
  }
`;

export default Style;
