import { styled } from "styled-components";

const Style = styled.div`
  color: white;
  background-color: var(--primary-color);
  font-size: var(--text-x-small);
  font-weight: var(--font-medium);
  padding: 0 2px 0 2px;
  border-radius: 4px;
  text-align: center;
  display: flex;
  align-items: center;
  div {
    font-size: var(--text-x-small);
    color: white;
  }
`;

export default Style;
