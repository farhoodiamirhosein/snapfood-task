/**
 * This component displays text beside between a counter world and percentage mark.
 * @param {number} value - Percentage number.
 */

import Style from "./index.style";
const PercentageBadge = ({ value }: { value: number }) => {
  return (
    <Style>
      تا{value}
      <div>%</div>
    </Style>
  );
};

export default PercentageBadge;
