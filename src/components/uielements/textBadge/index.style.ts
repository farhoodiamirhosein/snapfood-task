import { styled } from "styled-components";

const Style = styled.div<{ $textColor: string; $bgColor: string }>`
  background: ${(props) => props.$bgColor};
  display: flex;
  flex-direction: row;
  padding: 0px 6px 0px 6px;
  border-radius: 10px;
  align-items: center;
  font-size: var(--text-small);
  font-weight: var(--font-regular);
  color: ${(props) => props.$textColor};
`;

export default Style;
