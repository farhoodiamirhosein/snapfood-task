/**
 * This component displays text in custom colored badge .
 * @param {string} text - Text that gonna shown inside bade .
 * @param {string} color - Text color.
 * @param {string} bgColor - Badge background color.
 */

import Style from "./index.style";

const TextBadge = ({
  text,
  color,
  bgColor,
}: {
  bgColor: string;
  text: string;
  color: string;
}) => {
  return (
    <Style $textColor={color} $bgColor={bgColor}>
      {text}
    </Style>
  );
};

export default TextBadge;
