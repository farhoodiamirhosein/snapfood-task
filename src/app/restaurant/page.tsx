import { getRestaurants } from "@/apis/restaurants";
import RestaurantsList from "@/components/pages/restaurant/restaurantsList";

export default async function Page() {
  const data = await getRestaurants({});
  return (
    <div>
      <RestaurantsList data={data} />
    </div>
  );
}
