/**
 * Added in Next13 to wrap and being layout for all page.tsx/page.ts files.
 */

import type { Metadata } from "next";
import localFont from "next/font/local";
import StyledComponentsRegistry from "@/components/styledComponentsRegistery";
import ReduxProvider from "@/components/reduxProvider";
import GlobalStyles from "@/styles/globalStyles";

const iranSans = localFont({
  src: [
    {
      path: "../../public/fonts/IRANSansWeb.woff2",
      weight: "400",
      style: "normal",
    },
    {
      path: "../../public/fonts/IRANSansWeb_Light.woff2",
      weight: "500",
      style: "light",
    },
    {
      path: "../../public/fonts/IRANSansWeb_Medium.woff2",
      weight: "500",
      style: "medium",
    },
    {
      path: "../../public/fonts/IRANSansWeb_Bold.woff2",
      weight: "700",
      style: "bold",
    },
    {
      path: "../../public/fonts/IRANSans(FaNum)_Bold.ttf",
      weight: "700",
      style: "bold",
    },
    {
      path: "../../public/fonts/IRANSans(FaNum)_Light.ttf",
      weight: "600",
      style: "light",
    },
    {
      path: "../../public/fonts/IRANSans(FaNum)_Medium.ttf",
      weight: "500",
      style: "medium",
    },
  ],
  variable: "--font-iransans",
});

export const metadata: Metadata = {
  title:
    "سفارش آنلاین از رستوران‌ها، ‌شیرینی‌فروشی‌ها، کافی‌شاپ‌ها، سوپرمارکت‌ها،‌ نانوایی‌ها و ...",
  description:
    "اسنپ فود (زودفود قدیم) سامانه سفارش آنلاین غذا، شیرینی و خرید آنلاین از کافی شاپ و سوپرمارکت ها در تهران، کرج، شیراز، اصفهان، مشهد و سراسر ایران",
  icons: [
    {
      rel: "icon",
      type: "image/png",
      sizes: "32x32",
      url: "/favicons/favicon-32x32.png",
    },
    {
      rel: "icon",
      type: "image/png",
      sizes: "16x16",
      url: "/favicons/favicon-16x16.png",
    },
    {
      rel: "apple-touch-icon",
      sizes: "96x96",
      url: "/favicons/favicon-96x96.png",
    },
  ],
};

function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html dir="rtl">
      <ReduxProvider>
        <body className={iranSans.className}>
          <StyledComponentsRegistry>
            <GlobalStyles />
            {children}
          </StyledComponentsRegistry>
        </body>
      </ReduxProvider>
    </html>
  );
}

export default RootLayout;
