"use client";
import { styled } from "styled-components";

const Style = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  a {
    color: black;
    text-decoration: none;
  }
`;

export default Style;
