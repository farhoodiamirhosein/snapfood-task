/**
 * All restaurants API gonna be here
 */

import axios from "./axios";
import { AxiosResponse } from "axios";

export interface RestaurantType {
  id: number;
  title: string;
  description: string;
  logo: string;
  defLogo: string;
  isOpen: boolean;
  backgroundImage: string;
  menuUrl: string;
  discountValueForView: number;
  best_coupon: string;
  is_pro: boolean;
  is_food_party: boolean;
  rate: number;
  voteCount: number;
  deliveryFee: number;
  isZFExpress: boolean;
}

export interface RestaurantItemType {
  type: string;
  data: RestaurantType;
}

export const getRestaurants = async ({
  page = 0,
  page_size = 10,
  lat = 35.754,
  long = 51.328,
}: {
  page?: number;
  page_size?: number;
  lat?: number;
  long?: number;
}): Promise<AxiosResponse<{ finalResult: RestaurantItemType[] }>> => {
  try {
    const response: AxiosResponse<{ finalResult: RestaurantType[] }> =
      await axios.get(
        "https://snappfood.ir/mobile/v3/restaurant/vendors-list",
        {
          params: {
            page,
            page_size,
            lat,
            long,
          },
        }
      );

    if (response.status !== 200) {
      throw new Error("Failed to fetch data");
    }

    //@ts-ignore
    return response.data;
  } catch (error) {
    console.log(error, "err");
    throw error;
  }
};
