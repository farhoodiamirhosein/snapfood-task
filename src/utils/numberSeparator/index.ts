/**
 * This functions receives a number and returns it with separators.
 * @param {number} num - Original number.
 * @param {number} separatorCount - Determine However many numbers should put the separator.
 * @param {number} separatorCount - Separator to add between the numbers.
 */

function numberSeparator(
  num: number,
  separatorCount: number,
  separator: string
): string {
  if (separatorCount <= 0 || separatorCount >= 6) {
    throw new Error("Separator count should be between 1 and 5.");
  }

  const numStr = num.toString();
  const numDigits = numStr.length;
  const parts: string[] = [];

  for (let i = 0; i < numDigits; i += separatorCount) {
    const start = Math.max(numDigits - i - separatorCount, 0);
    const end = numDigits - i;
    parts.unshift(numStr.slice(start, end));
  }

  return parts.join(separator);
}

export default numberSeparator;
